﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL
{
    class Menu
    {
        public static int ShowMenu()
        {
            Console.WriteLine(new string('=',50));
            Console.WriteLine("Select the point");
            Console.WriteLine("1. Current parking balance");
            Console.WriteLine("2. The amount of money earned for the current period.");
            Console.WriteLine("3. The amount free/occupied parking places.");
            Console.WriteLine("4. All Parking Transactions for the current period.");
            Console.WriteLine("5. Transaction history.");
            Console.WriteLine("6. List of Vehicles located on the Parking.");
            Console.WriteLine("7. Add Vehicles to the Parking");
            Console.WriteLine("8. Remove the Vehicle from the Parking.");
            Console.WriteLine("9. Top up the balance of Vehicle");
            Console.WriteLine("0. End and Exit");
            Console.WriteLine(new string('=', 50));
            return Convert.ToInt32(Console.ReadLine());
        }

        internal void ReadFromLogMenu()
        {
            throw new NotImplementedException();
        }

        internal void GetVehiclesMenu()
        {
            throw new NotImplementedException();
        }

        internal void AddVenicleMenu()
        {
            throw new NotImplementedException();
        }

        internal void RemoveVehicleMenu()
        {
            throw new NotImplementedException();
        }

        internal void TopUpVehicleMenu()
        {
            throw new NotImplementedException();
        }

        internal void GetLastParkingTransactionsMenu()
        {
            throw new NotImplementedException();
        }

        internal void FreeOccupiedMenu()
        {
            throw new NotImplementedException();
        }

        internal void AmountMenu()
        {
            throw new NotImplementedException();
        }

        internal void CurrentBalanceMenu()
        {
            throw new NotImplementedException();
        }
    }
}
