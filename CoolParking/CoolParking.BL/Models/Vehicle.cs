﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private string id;
        private readonly string idPattern = @"[A-Z]{2}[-][0-9]{4}[-][A-Z]{2}";
        private VehicleType vehicleType;
        private decimal balance;

        public string Id
        {
            get => id;
            private set
            {
                if (Regex.IsMatch(value, idPattern))
                {
                    id = value;
                }
                else throw new ArgumentException();
            }
        }

        public VehicleType VehicleType
        {
            get => vehicleType;
            private set => vehicleType = value;
        }

        public decimal Balance
        {
            get => balance;
            private set
            {
                if (value > 0)
                {
                    balance = value;
                }
                else throw new ArgumentException();
            }
        }

        public void Withdraw(decimal sum)
        {
            if (sum > 0)
            {
                balance -= sum;
            }
            else throw new ArgumentException();
        }

        public void TopUp(decimal sum)
        {
            if (sum > 0)
            {
                balance += sum;
            }
            else throw new ArgumentException();
        }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            this.Id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;
        }

        public Vehicle(VehicleType vehicleType, decimal balance)
        {
            this.Id = GenerateRandomVehicleID();
            this.balance = Balance;
            this.VehicleType = vehicleType;
        }

        static string GenerateRandomVehicleID()
        {
            StringBuilder sb = new StringBuilder(10);
            Random rd = new Random();

            sb.AppendFormat(GenerateLetters());
            sb.AppendFormat(AddSeparator());
            sb.AppendFormat(GenerateNumbers());
            sb.AppendFormat(AddSeparator());
            sb.AppendFormat(GenerateLetters());

            return sb.ToString();

            string GenerateLetters()
            {
                string letters = "";
                letters += (char)rd.Next('A', 'Z');
                letters += (char)rd.Next('A', 'Z');
                return letters;
            }

            string GenerateNumbers()
            {
                string numbers = "";
                for (byte i = 0; i < 4; i++)
                {
                    numbers += rd.Next(0,9);
                }
                return numbers;
            }

            string AddSeparator()
            {
                return "-";
            }


        }
    }
}